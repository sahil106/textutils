import React, {useState} from 'react'

export default function TextForm(props) {
  const handleUpClick=()=>{
    let newText = text.toUpperCase();
    setText(newText);
    props.showAlert("Convert to upperCase!",'success')
  }

  const handleLoClick=()=>{
    let newText = text.toLowerCase();
    setText(newText);
    props.showAlert("Convert to lowerCase!",'success')
  }

  const handleClear=()=>{
    let newText = "";
    setText(newText);

  }

  const handleUndo=()=>{
    let newText = text.slice(0,text.length -1);
    setText(newText);
  }

  const handleExtraSpace =()=>{
   let newText =  text.split(/[ ]+/);
   setText(newText.join(" "))
  }




  const handleOnChange=(event)=>{
    setText(event.target.value);
    
  }
  const [text, setText]= useState("");

  return (
    <>
    <div className='container'  style={{color: props.mode === 'dark'?'white':'black'}}>
        <h1>{props.heading}</h1>
        <div className="mb-3">
        <textarea className="form-control" value={text} onChange={handleOnChange} style={{background: props.mode === 'dark'?'white':'light'}} id="myBox " rows="8"></textarea>
        </div>
        <button className='btn btn-primary mx-2'  onClick={handleUpClick}>Convert to UpperCase</button>
        <button className='btn btn-primary mx-2'  onClick={handleLoClick}>Convert to LowerCase</button>
        <button className='btn btn-primary mx-2'  onClick={handleClear}>Clear Text</button>
        <button className='btn btn-primary mx-2'  onClick={handleUndo}>Undo</button>
        <button className='btn btn-primary mx-2'  onClick={handleExtraSpace}>Remove Extra Space</button>
    </div>
    <div className='container'  style={{color: props.mode === 'dark'?'white':'black'}}>
      <h1>Your Text Summary</h1>
      <p>{text.split(" ").length} words and {text.length} characters</p>
      <p>{0.008 * text.split(" ").length} Minutes read</p>
      <h2>Preview</h2>
      <p>{text.length>0?text:"Enter something in the textbox to preview it here"}</p>
    </div>
    </>

  )
}
