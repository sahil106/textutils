import { useState } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import TextForm from './components/TextForm';
import Alert from './components/Alert';

function App() {
  const [mode, setMode]= useState('light')
  const [alert, setAlert] =useState(null)
   
   const showAlert=(message,type)=>{
      setAlert({
        msg:message,
        type:type
      })
      setTimeout(() => {
        setAlert(null)
      },1500);

   }
   const toggleMode = ()=>{
    if(mode === 'light'){
      setMode('dark');
      document.body.style.backgroundColor = 'black'
      showAlert("dark mode has been enabled", "success");
    }
    else{
      setMode('light');
      document.body.style.backgroundColor = 'white'
      showAlert("light mode has been enabled", "danger");
    }
    
    
   }

  return (
  <>
  <Navbar title = "TextUtils" aboutText="About Us" toggleMode={toggleMode} mode={mode} ></Navbar> 
  <Alert alert={alert}/>
  <div className="container ">
  <TextForm showAlert={showAlert} heading = "Enter the text to analyze below" mode={mode}></TextForm>
  </div>
  </>
  );
}

export default App;
